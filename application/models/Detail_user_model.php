<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Detail_user_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("detail_user", $data);
        }

        function read($where = "", $order = "", $limit = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($order)) $this->db->order_by($order);
            if(!empty($limit)) $this->db->limit($limit);

            $query = $this->db->get("detail_user");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("detail_user", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("detail_user");
        }
    }
?>