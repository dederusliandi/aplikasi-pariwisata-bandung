<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Artikel_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("artikel", $data);
        }

        function read($where = "", $order = "", $limit = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($order)) $this->db->order_by($order);
            if(!empty($limit)) $this->db->limit($limit);

            $this->db->select("artikel.*, user.nama_lengkap");
            $this->db->join('user', 'user.id = artikel.id_user');
            $query = $this->db->get("artikel");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("artikel", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("artikel");
        }
    }
?>