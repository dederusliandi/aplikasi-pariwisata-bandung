<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Wisata_model');
        $this->load->model('Artikel_model');
        $this->load->model('Event_model');
        $this->load->model('Kategori_model');
        $this->load->model('Penginapan_model');
        $this->load->model('Statistik_pengujung_model');
    }
    public function index()
    {
        $ip      = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
        $tanggal = date("Y-m-d"); // Mendapatkan tanggal sekarang
        $waktu = time();
        $data = $this->Statistik_pengujung_model->read(["ip" => $ip, "tanggal" => $tanggal]);
        if(!empty($data)) {
            $this->Statistik_pengujung_model->update(["ip" => $ip, "tanggal" => $tanggal],[
                "ip" => $ip,
                "tanggal" => $tanggal,
                "hits" => $data[0]->hits + 1,
                "online" => $waktu,
            ]);
        } else {
           $this->Statistik_pengujung_model->create([
               "ip" => $ip,
               "tanggal" => $tanggal,
               "hits" => 1,
               "online" => $waktu,
           ]);
        }
        
        $data['view'] = 'user/home/index';
        $data['data_wisata'] = $this->Wisata_model->read("wisata.status = 1","wisata.id DESC",5);
        $data['jumlah_wisata'] = $this->Wisata_model->read("wisata.status = 1");
        $data['data_artikel'] = $this->Artikel_model->read("","artikel.id DESC",5);
        $data['data_kategori'] = $this->Kategori_model->read();
        $this->load->view('user', $data);
    }

    public function artikel()
    {
        $data['view'] = 'user/home/artikel';
        $data['data_artikel_recent'] = $this->Artikel_model->read("","artikel.id DESC",5);
        $data['data_artikel'] = $this->Artikel_model->read("","artikel.id DESC");
        $this->load->view('user', $data);
    }

    public function detail_artikel($id)
    {
        $data['view'] = 'user/home/detail_artikel';
        $data['data_artikel_recent'] = $this->Artikel_model->read("","artikel.id DESC",5);
        $data['artikel'] = $this->Artikel_model->read("artikel.id = '$id'")[0];
        $this->load->view('user', $data);
    }

    public function event()
    {
        $data['view'] = 'user/home/event';
        $data['data_event_recent'] = $this->Event_model->read("","event.id DESC",5);
        $data['data_event'] = $this->Event_model->read("","event.id DESC");
        $this->load->view('user', $data);
    }

    public function detail_event($id)
    {
        $data['view'] = 'user/home/detail_event';
        $data['data_event_recent'] = $this->Event_model->read("","event.id DESC",5);
        $data['event'] = $this->Event_model->read("event.id = '$id'")[0];
        $this->load->view('user', $data);
    }

    public function wisata()
    {
        $data['view'] = 'user/home/wisata';
        $data['data_wisata_recent'] = $this->Wisata_model->read("wisata.status = 1","wisata.id DESC",5);
        $data['data_wisata'] = $this->Wisata_model->read("wisata.status = 1","wisata.id DESC");
        $this->load->view('user', $data);
    }

    public function detail_wisata($id)
    {
        $data['view'] = 'user/home/detail_wisata';
        $data['data_wisata_recent'] = $this->Wisata_model->read("wisata.status = 1","wisata.id DESC",5);
        $data['wisata'] = $this->Wisata_model->read("wisata.id = '$id'")[0];
        $this->load->view('user', $data);
    }

    public function search_wisata()
    {
        $id_kategori = $this->input->get('id_kategori');
        $harga = $this->input->get('harga');
        $data['harga_tiket'] = $harga;
        $data['view'] = 'user/home/search_wisata';
        $data['data_kategori'] = $this->Kategori_model->read();
        $data['data_wisata_recent'] = $this->Wisata_model->read("wisata.status = 1","wisata.id DESC",5);
        // $data['kategori'] = $this->Kategori_model->read("id = '$id_kategori'")[0];
        $data['data_wisata'] = $this->Wisata_model->readHarga(array("wisata.harga_tiket >=" => 0, "wisata.harga_tiket <=" => $harga),$id_kategori);
        $this->load->view('user', $data);
    }

    public function kategori_wisata($id)
    {
        $data['view'] = 'user/home/kategori_wisata';
        $data['data_wisata_recent'] = $this->Wisata_model->read("wisata.status = 1","wisata.id DESC",5);
        $data['kategori'] = $this->Kategori_model->read("id = '$id'")[0];
        $data['data_kategori'] = $this->Kategori_model->read();
        $data['data_wisata'] = $this->Wisata_model->read(array('wisata.status' => 1,'wisata.id_kategori' => $id), "wisata.id DESC");
        $this->load->view('user', $data);
    }

    public function penginapan()
    {
        $data['view'] = 'user/home/penginapan';
        $data['data_penginapan_recent'] = $this->Penginapan_model->read("","penginapan.id DESC",5);
        $data['data_penginapan'] = $this->Penginapan_model->read("","penginapan.id DESC");
        $this->load->view('user', $data);
    }

    public function detail_penginapan($id)
    {
        $data['view'] = 'user/home/detail_penginapan';
        $data['data_penginapan_recent'] = $this->Penginapan_model->read("","penginapan.id DESC",5);
        $data['penginapan'] = $this->Penginapan_model->read("penginapan.id = '$id'")[0];
        $this->load->view('user', $data);
    }

    public function search_penginapan()
    {
        $kategori = $this->input->get('kategori');
        $harga = $this->input->get('harga');
        $data['harga'] = $harga;
        $data['view'] = 'user/home/search_penginapan';
        $data['data_kategori'] = $this->Kategori_model->read();
        $data['data_penginapan_recent'] = $this->Penginapan_model->read("","penginapan.id DESC",5);
        $data['data_penginapan'] = $this->Penginapan_model->read(array("penginapan.harga >=" => 0, "penginapan.harga <=" => $harga, "penginapan.kategori" => $kategori));
        $this->load->view('user', $data);
    }

    

}
