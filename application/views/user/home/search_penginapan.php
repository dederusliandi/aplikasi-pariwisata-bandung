<section class="site-hero site-sm-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?= base_url("assets/user/images/penginapan.jpg") ?>);">
      <div class="container">
        <div class="row align-items-center justify-content-center site-hero-sm-inner">
          <div class="col-md-7 text-center">
  
            <div class="mb-5 element-animate">
              <p class="bcrumb"><a href="<?= site_url('/') ?>">Home</a> <span class="sep ion-android-arrow-dropright px-2"></span>  <span class="current">Penginapan</span></p>
            </div>
            
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          
          <div class="col-md-6 col-lg-8 order-md-2">
            <div class="row align-items-center justify-content-center site-hero-inner">
              <div class="col-md-10">
                <div class="mb-5 element-animate">
                  <div class="block-17">
                    <form action="<?= site_url('search_penginapan') ?>" method="get" class="d-block d-lg-flex mb-4">
                      <div class="fields d-block d-lg-flex">
                        <div class="textfield-search one-third"><input type="number" name="harga" value="<?= $this->input->get('harga') ?>" class="form-control" placeholder="Harga" required></div>
                        <div class="select-wrap one-third">
                          <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                          <select name="kategori" id="" class="form-control" required>
                            <option value="">[ Kategori penginapan ]</option>
                            <option value="1" <?= $this->input->get('kategori') == "1" ? "selected" : ""?>>Penginapan</option>
                            <option value="2" <?= $this->input->get('kategori') == "2" ? "selected" : ""?>>Hotel</option>
                          </select>
                        </div>
                      </div>
                      <input type="submit" class="search-submit btn btn-primary" value="Search">  
                    </form>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="row">
              <?php
                if(empty($data_penginapan)) {
                    ?>
                        <center><h4>Data Penginapan Kosong</h4></center>
                    <?php
                } else {
                    ?>
                        <h6>Pencarian Kategori dengan harga tiket maksimal <?= number_format($harga) ?></h6><br><br>
                    <?php
                    foreach($data_penginapan as $penginapan) {
                        ?>
                          <div class="col-md-12 col-lg-6 mb-5">
                            <div class="block-20 ">
                              <figure>
                                <a href="<?= site_url('penginapan/detail_penginapan/'.$penginapan->id) ?>"><img src="<?= base_url('uploads/penginapan/'.$penginapan->foto) ?>" alt="" class="img-fluid"></a>
                              </figure>
                              <div class="text">
                                <h3 class="heading"><a href="<?= site_url('penginapan/detail_penginapan/'.$penginapan->id) ?>"><?= $penginapan->nama ?></a></h3>
                                <div class="meta">
                                  <div><span class="ion-android-calendar"></span> <?= date('d F Y', strtotime($penginapan->waktu_upload)) ?></div>
                                  <div><span class="ion-android-person"></span> Harga Tiket: Rp. <?= number_format($penginapan->harga) ?></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php
                      }
                }
              ?>
            </div>

            <!-- <div class="row mb-5">
              <div class="col-md-12 text-center">
                <div class="block-27">
                  <ul>
                    <li><a href="#">&lt;</a></li>
                    <li class="active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&gt;</a></li>
                  </ul>
                </div>
              </div>
            </div> -->
          </div>
          <!-- END content -->
          <div class="col-md-6 col-lg-4 order-md-1">
            <div class="block-25 mb-5">
              <div class="heading">penginapan Lainnya</div>
              <ul>
                <?php
                  foreach($data_penginapan_recent as $penginapan) {
                    ?>
                      <li>
                        <a href="<?= site_url('penginapan/detail_penginapan/'.$penginapan->id) ?>" class="d-flex">
                          <figure class="image mr-3">
                            <img src="<?= base_url('uploads/penginapan/'.$penginapan->foto) ?>" alt="" class="img-fluid">
                          </figure>
                          <div class="text">
                            <h3 class="heading"><?= $penginapan->nama ?></h3>
                          </div>
                        </a>
                      </li>
                    <?php
                  }
                ?>
              </ul>
            </div>
          </div>
          <!-- END Sidebar -->
        </div>
      </div>
    </div>

    <div class="py-5 block-22">
        <div class="container">
            <div class="row align-items-center">
            <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
                <h2 class="heading">Menarik dengan konten kami ?</h2>
                <p>Silahkan daftarkan diri anda dan jadilah salah satu kontributor dalam konten penginapan bandung</p>
            </div>
            <div class="col-md-6">
                <form action="#" class="subscribe">
                <div class="form-group">
                    <a style="background-color:#fff; color:#11cbd7;" class="btn btn-primary submit" href="<?= site_url('auth/registrasi') ?>">Registrasi</a>
                </div>
                </form>
            </div>
            </div>
        </div>
        </div>