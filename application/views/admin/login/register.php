<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Aplikasi Wisata Bandung | Registrasi</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/style.css') ?>">
</head>
<body class="hold-transition">
    <div class="login-page">
        <div class="login-box" style="width:50%;">
            <form class="login-box-body"  method="POST" action="<?= site_url('auth/registrasi/do_registrasi') ?>" enctype="multipart/form-data">
                <div class="top">
                    <h2 class="login-logo">Registrasi Pemilik Wisata</h2>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input id="text" type="text" class="form-control" Placeholder="Nama Lengkap" name="nama_lengkap" value="" required autofocus>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input id="text" type="text" class="form-control" Placeholder="Username" name="username" value="" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input id="password" type="password" Placeholder="Password" class="form-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="">Konfirmasi Password</label>
                        <input id="password" type="password" Placeholder="Konfirmasi Password" class="form-control" name="konfirmasi_password" required>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="id_level"  value="2">
                        <!-- <select name="id_level" id="" class="form-control" required placeholder="Level">
                            <option value="">[ Pilih Level ]</option>
                            <option value="2">Pemilik Wisata</option>
                            <option value="3">Wisatawan</option>
                        </select> -->
                    </div>
                </div>
                <div class="bottom">
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-login btn-block" type="submit">Submit</button>
                            <br>
                            <center>
                                <a href="<?= site_url('auth/login') ?>">Login</a>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <footer class="footer">Copyright 2019 Aplikasi Pariwisata Bandung. All Right Reserved</footer>
    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
</body>
</html>
