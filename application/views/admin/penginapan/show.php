<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Penginapan
            <small>detail data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('auth/penginapan') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                            <tr>
                                <th width="200px">Nama Penginapan</th>
                                <td width="50px">:</td>
                                <td><?= $penginapan->nama ?></td>
                            </tr>

                            <tr>
                                <th>Foto</th>
                                <td>:</td>
                                <td><img src="<?= base_url('uploads/penginapan/'.$penginapan->foto) ?>" width="200px"/></td>
                            </tr>

                            <tr>
                                <th>Kategori</th>
                                <td>:</td>
                                <td>
                                    <?php
                                        if($penginapan->kategori == "1") {
                                            echo "Homestay";
                                        } else {
                                            echo "Hotel";
                                        }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <th>Harga</th>
                                <th>:</th>
                                <th>Rp. <?= number_format($penginapan->harga) ?></th>
                            </tr>

                            <tr>
                                <th>Google Maps</th>
                                <th>:</th>
                                <th><?= $penginapan->google_maps ?></th>
                            </tr>

                            <tr>
                                <th>Deskripsi</th>
                                <td>:</td>
                                <td><?= $penginapan->deskripsi ?></td>
                            </tr>
                        </table>    
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->