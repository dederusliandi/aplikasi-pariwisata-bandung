<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Penginapan
          <small>Edit Data</small>
      </h1>
  </section>

  <!-- Main content -->
  <section class="content">
        <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box">
                        <div class="box-header with-border">
                            <a href="<?= site_url('auth/penginapan') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form role="form" method="POST" action="<?= site_url('auth/penginapan/update/'.$penginapan->id)?>" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nama Penginapan</label>
                                    <input type="text" class="form-control" name="nama" placeholder="Nama penginapan" value="<?= $penginapan->nama ?>" required>
                                </div>

                                <div class="form-group">
                                    <label>Kategori Penginapan</label>
                                    <select name="kategori" id="" class="form-control" required>
                                        <option value="">[Pilih Kategori]</option>
                                        <option value="1" <?= $penginapan->kategori == "1" ? "selected" : "" ?>>Homestay</option>
                                        <option value="2" <?= $penginapan->kategori == "2" ? "selected" : "" ?>>Hotel</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" class="form-control" name="foto" placeholder="Foto">
                                </div>
                        
                                <div class="form-group">
                                    <label>Deskripsi</label>    
                                    <textarea name="deskripsi" id="editor1" cols="30" rows="10" class="form-control"><?= $penginapan->deskripsi ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Link Google Maps</label>
                                    <input type="text" class="form-control" name="google_maps" placeholder="Link Google Maps" required value="<?= html_escape($penginapan->google_maps) ?>">
                                </div>

                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="number" class="form-control" name="harga" placeholder="Harga" required value="<?= $penginapan->harga ?>">
                                </div>

                            </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->