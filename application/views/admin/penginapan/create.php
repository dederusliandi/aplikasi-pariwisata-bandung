<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Penginapan
          <small>Tambah Data</small>
      </h1>
  </section>

  <!-- Main content -->
  <section class="content">
        <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box">
                        <div class="box-header with-border">
                            <a href="<?= site_url('auth/penginapan') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form role="form" method="POST" action="<?= site_url('auth/penginapan/store')?>" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nama Penginapan</label>
                                    <input type="text" class="form-control" name="nama" placeholder="Nama Penginapan" required>
                                </div>

                                <div class="form-group">
                                    <label>Kategori Penginapan</label>
                                    <select name="kategori" id="" class="form-control" required>
                                        <option value="">[Pilih Kategori]</option>
                                        <option value="1">Homestay</option>
                                        <option value="2">Hotel</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" class="form-control" name="foto" placeholder="Foto" required>
                                </div>
                        
                                <div class="form-group">
                                    <label>Deskripsi</label>    
                                    <textarea name="deskripsi" id="editor1" class="form-control" style="height:500px;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Link Google Maps</label>
                                    <input type="text" class="form-control" name="google_maps" placeholder="Link Google Maps" required>
                                </div>
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="number" class="form-control" name="harga" placeholder="Harga" required>
                                </div>
                            </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->