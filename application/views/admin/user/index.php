<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                  <th>No</th>
                                  <th>nama lengkap</th>
                                  <th>Username</th>
                                  <th>Level</th>
                                  <th>Status</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($dataUser as $index => $user) {
                                        ?>
                                            <tr>
                                                <td><?= $index + 1 ?></td>
                                                <td><?= $user->nama_lengkap ?></td>
                                                <td><?= $user->username ?></td>
                                                <td>
                                                    <?php 
                                                        if($user->id_level == 1) {
                                                            echo "admin";
                                                        } elseif($user->id_level == 2) {
                                                            echo "pemilik wisata";
                                                        } else {
                                                            echo "wisatawan";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($user->status == 0) {
                                                            echo "<label class='label label-warning'>belum di acc</label>";
                                                        } else {
                                                            echo "<label class='label label-success'>di acc</label>";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if($user->status == 0) {
                                                            ?>
                                                                <a href="<?= site_url('auth/user/acc/'.$user->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Acc</a>
                                                            <?php
                                                        } 
                                                    ?>
                                                    <a href="<?= site_url('auth/user/show/'.$user->id) ?>" class="btn btn-info btn-sm"><i class="fa fa-th"></i></a>
                                                    <a href="<?= site_url('auth/user/delete/'.$user->id) ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->