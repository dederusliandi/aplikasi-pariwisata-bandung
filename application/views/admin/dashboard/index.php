<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <?php
            if($this->session->userdata("id_level") == "1") {
                ?>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><?= count($this->db->where('status', 1)->get('wisata')->result()); ?></h3>
                                    <p>Wisata</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h3><?= count($this->db->get('event')->result()); ?></h3>
                                    <p>Event</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><?= count($this->db->get('artikel')->result()); ?></h3>
                                    <p>Artikel</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Statistik Pengunjung</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                <div id="chartdiv-count" style="width:100%; height:400px;"></div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                <?php
            }

            if($this->session->userdata("id_level") == "2") {
                ?>
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><?= count($this->db->where('id_user', $this->session->userdata("id_user"))->where('status', 1)->get('wisata')->result()); ?></h3>
                                    <p>Wisata yang di ACC</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-blue">
                                <div class="inner">
                                <h3><?= count($this->db->where('id_user', $this->session->userdata("id_user"))->where('status', 0)->get('wisata')->result()); ?></h3>
                                    <p>Wisata yang belum di ACC</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>
                <?php
            }
        ?>

        <?php
            if($this->session->userdata("id_level") == "2") {
                ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Wisata yang diajukan</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Foto</th>
                                                <th>Nama Wisata</th>
                                                <th>Kategori</th>
                                                <th>User</th>
                                                <th>Status</th>
                                                <th>Alasan Tidak Diterima</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($dataWisata as $index => $data) {
                                                    ?>
                                                        <tr>
                                                            <td><?= $index + 1 ?></td>
                                                            <td>
                                                                <img src="<?= base_url('uploads/wisata/'.$data->foto_wisata) ?>" alt="" width="100px">
                                                            </td>
                                                            <td><?= $data->nama_wisata ?></td>
                                                            <td><?= $data->kategori_wisata ?></td>
                                                            <td><?= $data->user_nama_lengkap ?></td>
                                                            <td>
                                                                <?php
                                                                    if($this->session->userdata("id_level") == 1) {
                                                                        if($data->status_wisata == 0) {
                                                                            echo "<label class='label label-danger'>tidak aktif</label>";
                                                                        } else {
                                                                            echo "<label class='label label-success'>aktif</label>";                                                            
                                                                        }
                                                                    } else {
                                                                        if($data->status_wisata == 0) {
                                                                            echo "<label class='label label-warning'>menunggu di acc oleh admin</label>";
                                                                        } else {
                                                                            echo "<label class='label label-success'>di acc</label>";                                                            
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td><?= empty($data->alasan) ? "-" : $data->alasan ?></td>

                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                <?php
            }
        ?>


        <?php
            if($this->session->userdata("id_level") == "1") {
                ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Wisata Terbaru</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Foto</th>
                                                <th>Nama Wisata</th>
                                                <th>Kategori</th>
                                                <th>User</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($dataWisata as $index => $data) {
                                                    ?>
                                                        <tr>
                                                            <td><?= $index + 1 ?></td>
                                                            <td>
                                                                <img src="<?= base_url('uploads/wisata/'.$data->foto_wisata) ?>" alt="" width="100px">
                                                            </td>
                                                            <td><?= $data->nama_wisata ?></td>
                                                            <td><?= $data->kategori_wisata ?></td>
                                                            <td><?= $data->user_nama_lengkap ?></td>
                                                            <td>
                                                                <?php
                                                                    if($this->session->userdata("id_level") == 1) {
                                                                        if($data->status_wisata == 0) {
                                                                            echo "<label class='label label-danger'>tidak aktif</label>";
                                                                        } else {
                                                                            echo "<label class='label label-success'>aktif</label>";                                                            
                                                                        }
                                                                    } else {
                                                                        if($data->status_wisata == 0) {
                                                                            echo "<label class='label label-warning'>menunggu di acc oleh admin</label>";
                                                                        } else {
                                                                            echo "<label class='label label-success'>di acc</label>";                                                            
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Event Terbaru</h3>                    
                                </div>  
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Foto</th>
                                                <th>Judul Event</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($dataEvent as $index => $data) {
                                                    ?>
                                                        <tr>
                                                            <td><?= $index + 1 ?></td>
                                                            <td>
                                                                <img src="<?= base_url('uploads/event/'.$data->foto) ?>" alt="" width="100px">
                                                            </td>
                                                            <td><?= $data->nama ?></td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Artikel Terbaru</h3>                    
                                </div>  
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Foto</th>
                                                <th>Judul Artikel</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($dataArtikel as $index => $data) {
                                                    ?>
                                                        <tr>
                                                            <td><?= $index + 1 ?></td>
                                                            <td>
                                                                <img src="<?= base_url('uploads/artikel/'.$data->foto) ?>" alt="" width="100px">
                                                            </td>
                                                            <td><?= $data->nama ?></td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>

                    </div>
                <?php
            }
        ?>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>