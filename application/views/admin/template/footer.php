<footer class="main-footer">
    <div class="pull-right hidden-xs">  
    </div>
    <strong>Copyright &copy; 2019</strong>
    reserved.
</footer>
</div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="<?= base_url('assets')?>//bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= base_url('assets')?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?= base_url('assets')?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets')?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?= base_url('assets')?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url('assets')?>/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('assets')?>/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url('assets')?>/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
    $(function () {
        $('#example1').DataTable({
        "pageLength": 10,
        "ordering": false,
        })

        $('#example2').DataTable({
        "pageLength": 50,
        "ordering": false,
        })
    })
    </script>

    <script src="<?= base_url('assets/amcharts/amcharts.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/amcharts/serial.js') ?>" type="text/javascript"></script>


    <script>

            var chart;
            var chartData = [
                <?php
                    foreach($dataPengujung as $d)
                    echo '
                    {
                        "date": "'.date('d F Y', strtotime($d->tanggal)).'",
                        "visitor": '.$d->hits.'
                    },
                    ';
                ?>
            ];

        AmCharts.ready(function () {
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "date";
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 30;
            categoryAxis.gridPosition = "start";

            // value
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "visitor";
            graph.balloonText = "[[category]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.9;
            graph.fillColors = '#00a65a';
            chart.addGraph(graph);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chart.addChartScrollbar(chartScrollbar);

            chart.creditsPosition = "top-right";

            chart.write("chartdiv-count");
        });
    </script>

    <!-- CK Editor -->
    <script src="<?= base_url('assets/bower_components/ckeditor/ckeditor.js') ?>"></script>
    <script type="text/javascript">
        CKEDITOR.replace('editor1');
    </script>
</body>
</html>